// khai báo mảng numbers
let numbers = [];

// Thêm số từ người dùng nhập vào mảng
function addNumber() {
  const input = document.querySelector("#input-number");
  const value = parseInt(input.value);
  if (value != null && !isNaN(value)) {
    numbers.push(value);
    input.value = "";
    displayNumbers(numbers, "numberArray");
    document.querySelector("#numberSection").classList.remove("d-none");
  } else alert("Vui lòng nhập số nguyên vào");
  input.focus();
}

// hiển thị số vừa nhập
function displayNumbers(arr, id) {
  const targetElement = document.getElementById(id);
  targetElement.innerHTML = "";
  for (var i = 0; i < arr.length; i++) {
    const item = document.createElement("span");
    item.className = "btn btn-success shadow-sm mr-2";
    item.innerHTML = arr[i];
    targetElement.appendChild(item);
  }
}

// 1. Tính tổng các số dương trong mảng
function sumPositiveNumbers() {
  var positiveSum = 0;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] >= 0) {
      positiveSum += numbers[i];
    }
  }
  var output = document.getElementById("ketQua1");
  output.innerHTML = `<h5 class="mb-0">Tổng các số dương trong mảng là:  <span class="btn btn-info shadow-sm">${positiveSum}</span></h5>`;
}
// Tạo một biến tổng = 0.
// Sử dụng vòng lặp for và duyệt qua mảng để kiểm tra mỗi phần tử có lớn hơn 0 hay không.
// Nếu phần tử đó lớn hơn 0, cộng dòn phần tử đó với biến tổng.
// Sau đó, hiển thị kết quả trên giao diện người dùng.

// 2. Đếm số lượng số dương trong mảng
function countPositiveNumbers() {
  let count = 0;
  for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0) {
      count++;
    }
  }
  const output = document.getElementById("ketQua2");
  output.innerHTML = `<h5 class="mb-0">Số lượng số dương trong mảng là: <span class="btn btn-info shadow-sm">${count}</span></h5>`;
}
// Sử dụng vòng lặp for và duyệt qua mảng để kiểm tra mỗi phần tử có lớn hơn 0 hay không.
// Nếu phần tử đó lớn hơn 0, tăng biến đếm lên 1.
// Sau đó, hiển kết quả trên giao diện người dùng.

// 3. Tìm số nhỏ nhất trong mảng
function findMinNumber() {
  let minNumber = numbers[0];
  for (let i = 1; i < numbers.length; i++) {
    if (numbers[i] < minNumber) {
      minNumber = numbers[i];
    }
  }
  const output = document.getElementById("ketQua3");
  output.innerHTML = `<h5 class="mb-0">Số nhỏ nhất trong mảng là: <span class="btn btn-success shadow-sm">${minNumber}</span></h5>`;
}
// Gán phần tử đầu tiên cho biến minNumber
// Sử dụng vòng lặp for để duyệt qua mảng và so sánh từng phần tử với biên minNumber
// Nếu một phần tử khác trong mảng nhỏ hơn biến minNumber, gán giá trị của phần tử đó cho biến minNumber.
// Sau đó, hiển kết quả trên giao diện người dùng.

// 4. Tìm số dương nhỏ nhất
function findMinPositiveNumber() {
  let minPositiveNumber = Infinity;
  const output = document.getElementById("ketQua4");
  for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0 && numbers[i] < minPositiveNumber) {
      minPositiveNumber = numbers[i];
    }
  }
  if (minPositiveNumber === Infinity) {
    output.innerHTML = `<h5>Không có số dương trong mảng`;
  } else {
    output.innerHTML = `<h5>Số dương nhỏ nhất trong mảng là: <span class="btn btn-success shadow-sm">${minPositiveNumber}</span></h5>`;
  }
}

// 5. Tìm số chẵn cuối cùng trong mảng
function findLastEvenNumber() {
  const output = document.getElementById("ketQua5");
  let lastEvenNumber = `<h5>Số chẵn cuối cùng trong mảng là: <span class="btn btn-danger shadow-sm">-1</span></h5>`;
  for (let i = numbers.length - 1; i >= 0; i--) {
    if (numbers[i] % 2 === 0) {
      lastEvenNumber = `<h5>Số chẵn cuối cùng trong mảng là: <span class="btn btn-success shadow-sm">${numbers[i]}</span></h5>`;
      break;
    }
  }
  output.innerHTML = lastEvenNumber;
}
//Ở đây, chúng ta khởi tạo biến lastEvenNumber với giá trị ban đầu là -1.
// Sau đó, ta sử dụng vòng lặp for để duyệt mảng từ phần tử cuối cùng đến phần tử đầu tiên.
// Trong mỗi lần lặp, ta kiểm tra xem phần tử hiện tại có phải là số chẵn hay không bằng cách kiểm tra số dư của phần tử đó khi chia cho 2 có bằng 0 hay không. Nếu tìm thấy phần tử chẵn đầu tiên, ta lưu trữ giá trị đó vào biến lastEvenNumber và thoát khỏi vòng lặp bằng câu lệnh break.
// Nếu không tìm thấy phần tử chẵn nào, giá trị của lastEvenNumber vẫn là -1.

// 6. Đổi chỗ 2 số trong mảng
function swapValues() {
  var index1 = document.querySelector("#index1").value * 1;
  var index2 = document.querySelector("#index2").value * 1;
  var temp;
  temp = numbers[index1];
  numbers[index1] = numbers[index2];
  numbers[index2] = temp;
  displayNumbers(numbers, "mangKetQua6");
  const output = document.getElementById("ketQua6");
  output.classList.remove("d-none");
}

// 7. Sắp xếp mảng tăng dần
function sortAscending() {
  var newNumbers = numbers;
  let length = newNumbers.length;
  for (let i = 0; i < length - 1; i++) {
    for (let j = 0; j < length - i - 1; j++) {
      if (newNumbers[j] > newNumbers[j + 1]) {
        // Hoán đổi giá trị của hai phần tử
        let temp = newNumbers[j];
        newNumbers[j] = newNumbers[j + 1];
        newNumbers[j + 1] = temp;
      }
    }
  }
  displayNumbers(numbers, "mangKetQua7");
  const output = document.getElementById("ketQua7");
  output.classList.remove("d-none");
}
// Ở đoạn code trên, chúng ta sử dụng hàm bubbleSort() để sắp xếp mảng bằng phương pháp sắp xếp nổi bọt.
// Đầu vào của hàm này là một mảng và đầu ra là mảng đã được sắp xếp theo thứ tự tăng dần.

// Tìm số nguyên tố đầu tiên trong mảng
function isPrime(number) {
  if (number < 2) {
    return false;
  }

  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      return false;
    }
  }

  return true;
}

function findFirstPrimeNumber() {
  const output = document.getElementById("ketQua8");
  var result = `<h5>Số nguyên tố đầu tiên trong mảng là: <span class="btn btn-danger shadow-sm">-1</span></h5>`;
  for (let i = 0; i < numbers.length; i++) {
    if (isPrime(numbers[i])) {
      result = `<h5>Số nguyên tố đầu tiên trong mảng là: <span class="btn btn-success shadow-sm">${numbers[i]}</span></h5>`;
      break;
    }
  }
  output.innerHTML = result;
}
//Ở đây, hàm isPrime được sử dụng để kiểm tra xem một số có phải số nguyên tố hay không.
// Hàm findFirstPrimeNumber sử dụng vòng lặp để duyệt qua từng phần tử trong mảng, và kiểm tra xem nó có phải số nguyên tố hay không.
// Nếu tìm được số nguyên tố đầu tiên, hàm trả về giá trị đó, ngược lại trả về -1.

// 9. Đếm số lượng số nguyên trong mảng mới nhập thêm
var realArray = [];
function addRealNumber() {
  const input = document.querySelector("#input9");
  const value = input.value * 1;
  realArray.push(value);
  input.value = "";
  displayNumbers(realArray, "realArray");
  const output = document.getElementById("array9");
  output.classList.remove("d-none");
  input.focus();
}

function countInteger() {
  let count = 0;
  for (let i = 0; i < realArray.length; i++) {
    if (Number.isInteger(realArray[i])) {
      // Kiểm tra số nguyên
      count++;
    }
  }
  const output = document.getElementById("ketQua9");
  output.innerHTML = `<h5>Số lượng số nguyên trong mảng số thực là: <span class="btn btn-info shadow-sm">${count}</span></h5>`;
}
// Bước 1: Nhập mảng số thực và hiển thị ra màn hình
// Bước 2: Tạo biến đếm count ban đầu bằng 0.
// Bước 3: Duyệt qua từng phần tử trong mảng số thực, sử dụng phương thức Number.isInteger() để kiểm tra xem phần tử đó có phải số nguyên hay không, nếu phần tử đó là số nguyên thì tăng biến đếm lên 1.
// Bước 4: Hiển thị giá trị biến đếm ra màn hình.

// 10. So sánh số lượng số dương và số lượng số âm xem số nào nhiều hơn.
function comparePositiveAndNegativeNumbers() {
  let positiveCount = 0;
  let negativeCount = 0;
  const output = document.getElementById("ketQua10");

  for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0) {
      positiveCount++;
    } else if (numbers[i] < 0) {
      negativeCount++;
    }
  }

  if (positiveCount > negativeCount) {
    output.innerHTML = "<h5>Số lượng số dương nhiều hơn</h5>";
  } else if (positiveCount < negativeCount) {
    output.innerHTML = "<h5>Số lượng số âm nhiều hơn</h5>";
  } else {
    output.innerHTML = "<h5>Số lượng số dương và số lượng số âm bằng nhau</h5>";
  }
}
//Ở đây, hàm comparePositiveAndNegativeNumbers sử dụng vòng lặp để duyệt qua từng phần tử trong mảng và kiểm tra xem nó là số dương hay số âm.
// Nếu là số dương, ta tăng biến đếm số lượng số dương lên 1, ngược lại, ta tăng biến đếm số lượng số âm lên 1.
// Sau khi duyệt xong mảng, hàm so sánh số lượng số dương và số lượng số âm để xác định số nào nhiều hơn, và trả về kết quả tương ứng.
